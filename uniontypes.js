"use strict";
let a = 20;
let b = a;
let e = a;
// (a as number).toFixed(2);
//union type
let h = 0;
//  h = 10;
//  h = "10";
// h=true;
// enum
//literal type
let s;
s = 'md';
s = 'lg';
let s2 = "sm";
let obj;
obj = { name: 'sm' };
obj = { age: 10 };
let obj2;
obj2 = { name: 'sm', age: 10 };
let obj3;
obj3 = { name: 'sm' };
/// in 
if ('age' in obj2) {
    console.log('yes');
}
else {
    console.log('no');
}
//# sourceMappingURL=uniontypes.js.map