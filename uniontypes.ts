
let a:unknown  = 20;
let b:number = a as number;
let e:number = <number> a;

// (a as number).toFixed(2);


//union type

let h:number|string = 0;
//  h = 10;
//  h = "10";
// h=true;
// enum

//literal type

let s:'sm'|'md'|'lg';
s = 'md';
s = 'lg';

//types alias   

type Sizes = "sm"|"md"|"lg";

let s2:Sizes = "sm";

type OBJ = {name:string}|{age:number};

let obj:OBJ;

obj = {name:'sm'}

obj = {age:10};

type OBJ2 = {name:string} & {age:number};
let obj2:OBJ2;

obj2 = {name:'sm',age:10};

type OBJ3 = {name:String,age?:number};

let obj3:OBJ3;

obj3 = {name:'sm'};

/// in 

if('age' in obj2){
    console.log('yes');
}else{
    console.log('no');
}